from flask import Blueprint, jsonify, request


_PEOPLE = {p: f'Person {p}' for p in range(10)}

people = Blueprint('people', __name__, url_prefix='/people')


@people.route('/', methods=['GET'])
def index():
    return '\n'.join(_PEOPLE.values()), 200


@people.route('/<int:person_id>', methods=['GET'])
def person(person_id):
    if person_id not in _PEOPLE:
        return jsonify({'message': f'Person {person_id} not found'}), 404

    return jsonify({'message': f'Person {person_id} returned'}), 200


@people.route('/add', methods=['POST'])
def people_add():
    person = request.values.get('person')

    if not person:
        return 'Person is required', 400

    if person in set(_PEOPLE.values()):
        return f'Person {person} is already in People collection', 400

    person_id = max(_PEOPLE.keys()) + 1
    _PEOPLE[person_id] = person
    return jsonify({
        'message': 'Success',
        'person': person,
        'person_id': person_id,
    })
