from flask import Flask, request, jsonify, url_for, abort
from people.people import people
from helpers import has_no_empty_params


SECRET_KEY = '8uk2emnasndkajshdku3oiiei1h2hemsdmz928282ksks'


app = Flask(__name__)
app.register_blueprint(people)


@app.before_request
def before_request():
    secret = request.args.get('secret')

    if secret != SECRET_KEY:
        abort(400, 'Bad secret key given')


@app.route('/', methods=['GET'])
def index():
    response = jsonify({
        'message': 'Home page',
    })
    # Add additional header in the response headers
    response.headers['x-ver'] = 'Custom version 1'
    return response, 200


@app.route('/site-map', methods=['GET'])
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links.append((url, rule.endpoint))
    # links is now a list of url, endpoint tuples
    return jsonify({
        'site_map': links,
    }), 200


if __name__ == '__main__':
    app.run()
